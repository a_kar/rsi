
package org.pb.rsi;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "WSService", targetNamespace = "http://rsi.pb.org/", wsdlLocation = "http://localhost:8080/WS-SSL/WSService?wsdl")
public class WSService
    extends Service
{

    private final static URL WSSERVICE_WSDL_LOCATION;
    private final static WebServiceException WSSERVICE_EXCEPTION;
    private final static QName WSSERVICE_QNAME = new QName("http://rsi.pb.org/", "WSService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/WS-SSL/WSService?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        WSSERVICE_WSDL_LOCATION = url;
        WSSERVICE_EXCEPTION = e;
    }

    public WSService() {
        super(__getWsdlLocation(), WSSERVICE_QNAME);
    }

    public WSService(WebServiceFeature... features) {
        super(__getWsdlLocation(), WSSERVICE_QNAME, features);
    }

    public WSService(URL wsdlLocation) {
        super(wsdlLocation, WSSERVICE_QNAME);
    }

    public WSService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, WSSERVICE_QNAME, features);
    }

    public WSService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WSService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns WS
     */
    @WebEndpoint(name = "WSPort")
    public WS getWSPort() {
        return super.getPort(new QName("http://rsi.pb.org/", "WSPort"), WS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WS
     */
    @WebEndpoint(name = "WSPort")
    public WS getWSPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://rsi.pb.org/", "WSPort"), WS.class, features);
    }

    private static URL __getWsdlLocation() {
        if (WSSERVICE_EXCEPTION!= null) {
            throw WSSERVICE_EXCEPTION;
        }
        return WSSERVICE_WSDL_LOCATION;
    }

}
