/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.ssl.klient;

import java.net.ProxySelector;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import org.pb.rsi.WS;
import org.pb.rsi.WSService;

/**
 *
 * @author alicj
 */
public class WSSSLKlient {

    public static void main(String[] args) {
        
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
        ProxySelector.setDefault(new CustomProxySelector());
//        System.setProperty("javax.debug", "all");
        
        System.setProperty("javax.net.ssl.trustStore", "./client/client_cacerts.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
        
        WSService service = new WSService();        
        WS port = service.getWSPort();
        
        // Authentication
        BindingProvider bindProv = (BindingProvider) port;
        Map<String, Object> context = bindProv.getRequestContext();
        context.put("javax.xml.ws.security.auth.username", "peter");
        context.put("javax.xml.ws.security.auth.password", "qwerty1");
        
        String echo = port.echo("Alicja");
        
        System.out.println("hello " + echo);  
    
}
}
