package org.pb.rsi;

import javax.jws.WebMethod;
import javax.jws.WebService;


@WebService
public class WS {
    
    @WebMethod()
    public String echo(String text) {
        return "Hello " + text;
    }
        
}
