/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.mtom.pkg1.client;


import java.net.ProxySelector;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.xml.ws.soap.MTOMFeature;
import myservice.ImageServerImpl;
import myservice.ImageServerImplService;

/**
 *
 * @author alicj
 */
public class WSMTOM1Client {

    public static void main(java.lang.String[] args) {
        
        ProxySelector.setDefault(new CustomProxySelector());
        
        ImageServerImplService service = new ImageServerImplService();        
        ImageServerImpl port = service.getImageServerImplPort(new MTOMFeature());
        
        byte[] image = port.downloadImage("cat.jpg");
        
        String wyraz = port.echo("alice");
        
        JFrame frame = new JFrame();
        frame.setSize(300, 300);
        JLabel label = new JLabel(new ImageIcon(image));
        frame.add(label);
        frame.setVisible(true);
        
        System.out.println("imageServer.downloadImage() : Download Successful!");
        System.out.println("wyraz:"+wyraz);
       
    }
    
    
}
