/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
 
public class RMIChatClient {
    public static void main (String[] argv) {
        try {
                    System.setProperty("java.security.policy", "security.policy");
                    //System.setProperty("java.rmi.server.hostname","192.168.0.198");
                    
                    System.setSecurityManager(new RMISecurityManager());
                    Scanner s=new Scanner(System.in);
                    System.out.println("Enter Your name and press Enter:");
                    String name=s.nextLine().trim();		    		    	
                    ChatInterface client = new Chat(name);

                    //ChatInterface server = (ChatInterface)Naming.lookup("rmi://192.168.0.198:1099/ABC");
                    ChatInterface server = (ChatInterface)Naming.lookup("//192.168.0.198:1099/ABC");
                    String msg="["+client.getName()+"] got connected";
                    server.send(msg);
                    System.out.println("[System] Chat Remote Object is ready:");
                    server.setClient(client);

                    while(true){
                            msg=s.nextLine().trim();
                            msg="["+client.getName()+"] "+msg;		    		
                            server.send(msg);
                    }

            }catch (Exception e) {
                    System.out.println("[System] Server failed: " + e);
            }
            }
}
