/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
 
public class RMIChatServer {
public static void main (String[] argv) {
    try {
                System.setProperty("java.rmi.server.hostname","192.168.0.198");
                System.setProperty("java.security.policy", "security.policy");
                System.setProperty("java.rmi.server.codebase", "file:/D:/NetBeansProjects/rsi_4_chat/RMIChatServer/target/classes");
            
	    	System.setSecurityManager(new RMISecurityManager());
	    	Scanner s=new Scanner(System.in);
	    	System.out.println("Enter Your name and press Enter:");
	    	String name=s.nextLine().trim();
                
                
                LocateRegistry.createRegistry(1099);
                
                Chat server = new Chat(name);

                Registry registry = LocateRegistry.getRegistry();
	    	Naming.rebind("//localhost/ABC", server);         
                //Naming.rebind("//192.168.0.198/ABC", server);
 
	    	System.out.println("[System] Chat Remote Object is ready:");
 
	    	while(true){
	    		String msg=s.nextLine().trim();
	    		if (server.getClient()!=null){
	    			ChatInterface client=server.getClient();
	    			msg="["+server.getName()+"] "+msg;
	    			client.send(msg);
	    		}	
	    	}
 
    	}catch (Exception e) {
    		System.out.println("[System] Server failed: " + e);
    	}
	}
}

