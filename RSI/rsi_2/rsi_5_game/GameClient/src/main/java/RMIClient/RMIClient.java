/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMIClient;
import java.rmi.Naming;
import java.util.Scanner;
import RMIServer.RemoteObject;
import RMIServer.ObjectInterface;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.rmi.ssl.SslRMIClientSocketFactory;
import java.net.*;

public class RMIClient {

    /**
     * @param args the command line arguments
     */
   // private static final int PORT = 1099;
    public static void main(String[] args) {
        System.setProperty("java.security.policy", "security.policy");
//        
        System.setSecurityManager(new SecurityManager());      

        try {
            
            //setSettings();
            
//            Registry registry = LocateRegistry.getRegistry(
//                InetAddress.getLocalHost().getHostName(), PORT,
//                new SslRMIClientSocketFactory());
            
            ObjectInterface remoteObject = (ObjectInterface) Naming.lookup("//localhost/ABC");
            
            Scanner scan = new Scanner(System.in);
            
            System.out.println("Working");
            
            RemoteObject playerObject = new RemoteObject();
            int id = remoteObject.addPlayer(playerObject);
            
            String signs[] = new String[] {"O", "X"};
            
            System.out.println("Twój znak to: " + signs[id]);
            
            while(true)
            {
                int x = scan.nextInt() - 1;
                int y = scan.nextInt() - 1;
                
                remoteObject.makeMove(id, x, y);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void setSettings(){
        
        String pass = "123456";
        System.setProperty("javax.net.ssl.trustStore", "myTrustStore.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", pass);
        
    }
    
}

