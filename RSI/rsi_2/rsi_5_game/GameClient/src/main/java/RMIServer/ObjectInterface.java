/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMIServer;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ObjectInterface extends Remote{
    int addPlayer(ObjectInterface player) throws RemoteException;
    void sendMessage(String message) throws RemoteException;
    ObjectInterface getPlayer(int i) throws RemoteException;
    void drawPool(String[][] pool, Boolean first, int actualMove, int winner) throws RemoteException;
    void makeMove(int i, int x, int y) throws RemoteException;
    int checkWinner() throws RemoteException;
}
