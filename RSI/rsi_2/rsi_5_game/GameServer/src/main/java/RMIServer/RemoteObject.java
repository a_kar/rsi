/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMIServer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import javax.rmi.ssl.*;

public class RemoteObject extends UnicastRemoteObject implements ObjectInterface {
    
    //private static final int PORT = 1099;
    
    int i = -1, actualMove = 0, winner = 0;
    String signs[] = new String[] {"O", "X"};
    
    String pool[][] = new String[][]{
        {" ", " ", " "},
        {" ", " ", " "},
        {" ", " ", " "}
    };
    
    ObjectInterface player1 = null;
    ObjectInterface player2 = null;
    
//    public RemoteObject() throws RemoteException {
//        //super();
//        super(PORT,
//                new SslRMIClientSocketFactory(),
//                new SslRMIServerSocketFactory());
//    }
    public RemoteObject() throws RemoteException {
        super();
    }

    @Override
    public int addPlayer(ObjectInterface player) throws RemoteException {
        if(i == -1) this.player1 = player;
        else if(i == 0) this.player2 = player;
        player.sendMessage("Nowy gracz dołączył do gry!");
        if(i == 0) {
            this.player1.sendMessage("Start rozgrywki!");
            this.player2.sendMessage("Start rozgrywki!");
            this.player1.drawPool(this.pool, true, this.actualMove, this.winner);
            this.player2.drawPool(this.pool, true, this.actualMove, this.winner);
        }
        i++;
        return i;
    }

    @Override
    public void sendMessage(String message) throws RemoteException {
        System.out.println("> " + message);
    }

    @Override
    public ObjectInterface getPlayer(int i) throws RemoteException {
        if(i == 0) return this.player1;
        else return this.player2;
    }

    @Override
    public void drawPool(String[][] pool, Boolean first, int actualMove, int winner) throws RemoteException {
        if(!first)
        {
            System.out.print("\033[H\033[2J");  
            System.out.flush(); 
        }
        
        System.out.println("Ruch gracza: " + this.signs[actualMove] + "\n");
        System.out.println("     1   2   3  ");
        System.out.println("   +---+---+---+");
        System.out.println(" 1 | " + pool[0][0] + " | " + pool[0][1] + " | " + pool[0][2] + " |");
        System.out.println("   +---+---+---+");
        System.out.println(" 2 | " + pool[1][0] + " | " + pool[1][1] + " | " + pool[1][2] + " |");
        System.out.println("   +---+---+---+");
        System.out.println(" 3 | " + pool[2][0] + " | " + pool[2][1] + " | " + pool[2][2] + " |");
        System.out.println("   +---+---+---+\n");
        
        if(winner == 0) System.out.println("Wybierz pole (napisz nr wiersza, a potem nr kolumny):");
        else
        if(winner == 1) System.out.println("Gracz O wygrał!!!");
        else
        if(winner == 2) System.out.println("Gracz X wygrał!!!");
        else
        if(winner == 3) System.out.println("Remis!!");
    }

    @Override
    public void makeMove(int i, int x, int y) throws RemoteException {
        if(this.winner != 0)
        {
            if(i == 0) this.player1.drawPool(this.pool, false, this.actualMove, this.winner);
            if(i == 1) this.player2.drawPool(this.pool, false, this.actualMove, this.winner);
            return;
        }
        
        if(i != this.actualMove)
        {
            if(i == 0) this.player1.drawPool(this.pool, false, this.actualMove, this.winner);
            if(i == 1) this.player2.drawPool(this.pool, false, this.actualMove, this.winner);
            return;
        }
        
        if(this.pool[x][y].equals(" "))
        {
            this.actualMove = (this.actualMove + 1) % 2;
            this.pool[x][y] = this.signs[i];
            
            this.winner = this.checkWinner();
            
            this.player1.drawPool(this.pool, false, this.actualMove, this.winner);
            this.player2.drawPool(this.pool, false, this.actualMove, this.winner);
        }
        else
        {
            if(i == 0) this.player1.drawPool(this.pool, false, this.actualMove, this.winner);
            if(i == 1) this.player2.drawPool(this.pool, false, this.actualMove, this.winner);
        }
    }

    @Override
    public int checkWinner() throws RemoteException {
        int unfilled = 0;
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                if(this.pool[i][j].equals(" ")) unfilled++;
        
        if(unfilled == 0) return 3;
        
        int[][][] positions = new int[][][]{
            { {0, 0, 0}, {0, 1, 2} },
            { {1, 1, 1}, {0, 1, 2} },
            { {2, 2, 2}, {0, 1, 2} },
            { {0, 1, 2}, {0, 0, 0} },
            { {0, 1, 2}, {1, 1, 1} },
            { {0, 1, 2}, {2, 2, 2} },
            { {0, 1, 2}, {0, 1, 2} },
            { {0, 1, 2}, {2, 1, 0} }
        };
        
        for(int k = 0; k < 8; k++)
        {
            int x1 = positions[k][0][0], y1 = positions[k][1][0];
            int x2 = positions[k][0][1], y2 = positions[k][1][1];
            int x3 = positions[k][0][2], y3 = positions[k][1][2];
            
            if(this.pool[x1][y1].equals(this.pool[x2][y2]) && this.pool[x2][y2].equals(this.pool[x3][y3]))
            {
                if(this.pool[x1][y1].equals("O")) return 1;
                if(this.pool[x1][y1].equals("X")) return 2;
            }
        }
        
        return 0;
    }   
    
    
}
