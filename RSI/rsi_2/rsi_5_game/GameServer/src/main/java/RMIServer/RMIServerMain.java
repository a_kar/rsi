/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMIServer;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

public class RMIServerMain {
    /**
     * @param args the command line arguments
     */
    //private static final int PORT = 1099;
    
    public static void main(String[] args) {
        try {
            //setSettings();
            
            System.setProperty("java.security.policy", "security.policy");
            System.setProperty("java.rmi.server.hostname", "192.168.0.198");
            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new SecurityManager());
            }
            
            System.setProperty("java.rmi.server.codebase","file:/D:/NetBeansProjects/rsi_5_game/GameServer/target/classes");

            LocateRegistry.createRegistry(1099);
            
//            Registry registry = LocateRegistry.createRegistry(
//                    PORT,
//                    new SslRMIClientSocketFactory(),
//                    new SslRMIServerSocketFactory());
           // System.out.println("RMI registry running on port " + PORT);

            RemoteObject obj = new RemoteObject();
            //Naming.rebind("//localhost/ABC", obj);
            Naming.rebind("//192.168.0.198/ABC", obj);
            
            Registry registry = LocateRegistry.getRegistry();
	    //Naming.rebind("rmi://localhost/ABC", obj);     
        }
        catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
    
//    private static void setSettings(){
//        
//        String pass = "123456";
//        System.setProperty("javax.net.ssl.keyStore", "myKeyStore.jks");
//        System.setProperty("javax.net.ssl.keyStorePassword", pass);
//        
//    }
    
}

