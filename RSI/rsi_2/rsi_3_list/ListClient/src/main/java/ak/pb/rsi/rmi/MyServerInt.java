/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.rmi;

import ak.pb.rsi.model.Event;
import ak.pb.rsi.model.Person;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author alicj
 */
public interface MyServerInt extends Remote {
    
    List<Person> listaOsob() throws RemoteException;
    
    List<Event> getEventList() throws RemoteException; 
    
    //public Event addNewEvent();
    
    public String getDescription (String text) throws RemoteException;

}
