/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.rmi;

/**
 *
 * @author alicj
 */
import ak.pb.rsi.model.Event;
import ak.pb.rsi.model.Person;
import java.rmi.Naming;
import java.util.List;

public class MyClientMain {

    public static void main(String[] args) {

        System.setProperty("java.security.policy", "security.policy");
        System.setSecurityManager(new SecurityManager());

        try {
            
            //MyServerInt myRemoteObject = (MyServerInt) Naming.lookup("//192.168.0.198:1099/ABC");
            
            //Calculator c = (Calculator) Naming.lookup("//192.168.0.198:1099/Calculator");
            //MyServerInt myRemoteObject = (MyServerInt) Naming.lookup("//localhost/ABC");
            MyServerInt myRemoteObject = (MyServerInt) Naming.lookup("//192.168.0.198:1099/ABC");
            String text = "Hello :-)";

            String result = myRemoteObject.getDescription(text);

            System.out.println("Wysłano do serwera: " + text);
            System.out.println("Otrzymana z serwera odpowiedź: " + result);

            /*
            List<Person> listaOsob = myRemoteObject.listaOsob();
            System.out.println("-------------------");
            for (Person next : listaOsob) {
                System.out.print("Imie: " + next.getImie());
                System.out.print("\t Nazwisko: " + next.getNazwisko());
                System.out.print("\t Wiek: " + next.getWiek() + "\n");
            }
            System.out.println("-------------------");
            */
            
            
            List<Event> eventList = myRemoteObject.getEventList();
            System.out.println("-------------------");
            for (Event next : eventList) {
                System.out.print("Nazwa: " + next.getName());
                System.out.print("\t Typ: " + next.getType() + "\n");
                System.out.print("\t data: " + next.getDate());
                System.out.print("\t week: " + next.getWeekNumber());
                System.out.print("\t month: " + next.getMonthNumber());
                System.out.print("\t year: " + next.getYearNumber());
                System.out.print("\t Opis: " + next.getDescritpion() + "\n");
            }
            System.out.println("-------------------");
            
            
            
        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}