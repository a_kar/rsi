/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.rmi;

/**
 *
 * @author alicj
 */
import ak.pb.rsi.model.Event;
import ak.pb.rsi.model.Person;
import ak.pb.rsi.rmi.MyServerInt;
import java.rmi.RemoteException;

import java.rmi.server.UnicastRemoteObject;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyServerImpl extends UnicastRemoteObject 
        implements MyServerInt {

    int i = 0;
    

    protected MyServerImpl() throws RemoteException {
        super();
    }
    
    @Override
    public List<Person> listaOsob() throws RemoteException {
        ArrayList<Person> arrayList = new ArrayList<Person>();
        arrayList.add(new Person("Natalia", "Kowalska", 13));
        arrayList.add(new Person("Viper", "Dragon", 16));
        arrayList.add(new Person("Zbigniew", "Laskowski", 14));
        
        return arrayList;
    }
    
    
    @Override
    public List<Event> getEventList() throws RemoteException {
        try {
            ArrayList<Event> arrayList = new ArrayList<>();
            arrayList.add(new Event("Wydarzenie 1", "koncert", "01.01.2021", "super koncert"));
            arrayList.add(new Event("Wydarzenie 2", "wystawa", "22.01.2021", "super koncert"));
            arrayList.add(new Event("Wydarzenie 3", "koncert", "10.02.2021", "super koncert"));
            arrayList.add(new Event("Wydarzenie 3", "koncert", "31.12.2020", "super koncert"));
            arrayList.add(new Event("Wydarzenie 3", "koncert", "31.12.2021", "super koncert"));
            
            return arrayList;
        } catch (ParseException ex) {
            Logger.getLogger(MyServerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    

    @Override
    public String getDescription(String text) throws RemoteException {
        i++;
        System.out.println("MyServerImpl.getDescription: " + text + " " + i);
        return "getDescription: " + text + " " + i;
    }
}
