/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Event implements Serializable {
    
    String name;
    String type;
    Date date;
    int weekNumber;
    int monthNumber;
    int yearNumber;
    String description;  
            
    public Event(String name,
        String type,
        String date_string,
        String description ) throws ParseException {
               
        this.name = name;
        this.type = type;
        this.description = description;
        
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        this.date = format.parse(date_string);
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        
        this.weekNumber = cal.get(Calendar.WEEK_OF_YEAR);
        this.monthNumber = cal.get(Calendar.MONTH)+1;
        this.yearNumber = cal.get(Calendar.YEAR);        
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String imie) {
        this.name = name;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public Date getDate() {
        return date;
    }
    
    public void setDate(String date_string) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd.mm.yyyy");
        this.date = format.parse(date_string);
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        
        this.weekNumber = cal.get(Calendar.WEEK_OF_YEAR);
        this.monthNumber = cal.get(Calendar.MONTH)+1;
        this.yearNumber = cal.get(Calendar.YEAR);
    }
    
    public String getDescritpion() {
        return description;
    }
    
    public void setDescription(String description) {
        this.name = description;
    }
    
    public int getWeekNumber() {
        return weekNumber;
    }
    
    public int getMonthNumber() {
        return monthNumber;
    }
    
    public int getYearNumber() {
        return yearNumber;
    }
    
}

