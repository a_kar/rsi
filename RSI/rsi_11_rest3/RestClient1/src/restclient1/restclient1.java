/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restclient1;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.Message;

public class restclient1 {
    public static void main(String[] args) {
        System.out.println("----------\nget1():"); 
        get1();
        System.out.println("----------\npost1():");
        post1();
        System.out.println("----------\nget3():");
        get3();
        System.out.println("----------\ngetZaczynaSie():");     
        getZaczynaSie("d");
//        System.out.println("----------\nget2():");
//        get2();
        
    }
    
    private static void get1(){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/RestWS1/webresources/messages/1");
        
        String message = target.request(MediaType.APPLICATION_JSON).get(String.class);
        System.out.println("response: "+ message);
    }
    
//    private static void get2(){
//        Client client = ClientBuilder.newClient();
//        WebTarget target = client.target("http://localhost:8080/RestWS1/webresources/messages/1");
//        Response response = target.request().get();
//        System.out.println("response: "+ response);
//        Message message = response.readEntity(Message.class);                
//        System.out.println("message: "+ message.toString());
//    }
    
    private static void post1(){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/RestWS1/webresources/messages/");
        
        Message newMessage = new Message(5, "Wiadomosc utworzona przez klienta", "Tadeusz");           
        Response response = target.request().post(Entity.json(newMessage));
        System.out.println("response: "+ response);
    }
        
    private static void get3(){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/RestWS1/webresources/messages/json");
        String message = target.request(MediaType.APPLICATION_JSON).get(String.class);
        System.out.println("response: "+ message);
//        List<Message> response = target
//                .request(MediaType.APPLICATION_JSON)
//                .get(new GenericType<List<Message>>(){ });
//        System.out.println("response: "+ response);
    }
    
    
    private static void getZaczynaSie(String s){
        Client client = ClientBuilder.newClient();
        String targetName = "http://localhost:8080/RestWS1/webresources/messages?zaczynasie="+s;
        WebTarget target = client.target(targetName);
        String message = target.request(MediaType.APPLICATION_JSON).get(String.class);
        System.out.println("response: "+ message);
    }
    
    
    
}
