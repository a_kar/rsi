/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myclient;

import java.net.ProxySelector;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import static javax.ws.rs.client.Entity.entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.Message;
import myclasses.Produkt;
import myclasses.ResponseList;
import myclasses.SearchParam;

/**
 * Jersey REST client generated for REST resource:SklepResource [/sklep]<br>
 * USAGE:
 * <pre>
 *        Sklep client = new Sklep();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author alicj
 */
public class Sklep {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/Rest-Produkty/webresources";

    public Sklep() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("sklep");
    }

    public <T> T getByNazwa(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("bynazwa");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public <T> T getAllProduktyXml(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("allproducts");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public <T> T findProducts(Object requestEntity, Class<T> responseType) throws ClientErrorException {
        return webTarget.path("findProducts").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), responseType);
    }

    public <T> T getProducts(Class<T> responseType, String onazwie) throws ClientErrorException {
        WebTarget resource = webTarget;
        if (onazwie != null) {
            resource = resource.queryParam("onazwie", onazwie);
        }
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public void close() {
        client.close();
    }
        
    public ResponseList findSearchProducts(String name, String producent, int cena) {
        WebTarget resource = webTarget;
        resource = resource.path("findProducts");
        
        SearchParam searchParam = new SearchParam();
        searchParam.setNazwa(name);
        searchParam.setProducent(producent);
        searchParam.setCenaMniejszaNiz(cena);
        
        ResponseList responseList;
        responseList = resource.request(MediaType.APPLICATION_XML)
                .post(Entity.entity(searchParam, MediaType.APPLICATION_JSON), 
                ResponseList.class);
        return responseList;
    }
    public ResponseList findSearchProducts2(String name, String producent, int cena) {
        WebTarget resource = webTarget;
        resource = resource.path("findProducts");
        
        SearchParam searchParam = new SearchParam();
        searchParam.setNazwa(name);
        searchParam.setProducent(producent);
        searchParam.setCenaMniejszaNiz(cena);
        
        ResponseList responseList;
        responseList = resource.request(MediaType.APPLICATION_XML)
                .post(Entity.entity(searchParam, MediaType.TEXT_PLAIN), ResponseList.class);
        return responseList;
    }
    
    public ResponseList searchList(){
        
        SearchParam searchParam1 = new SearchParam();
        searchParam1.setNazwa("mysz");
        searchParam1.setProducent("acer");
        searchParam1.setCenaMniejszaNiz(200);
        
        ResponseList responseList;
        responseList =  webTarget.path("findProducts").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(Entity.json(searchParam1), 
                javax.ws.rs.core.MediaType.APPLICATION_XML), ResponseList.class);
        return responseList;
    
    }
    public static void main(String[] args){
        
        ProxySelector.setDefault(new CustomProxySelector());
        
        System.out.println("start");
        System.out.println("------------------");
        
        Sklep client = new Sklep();
        ResponseList allProdukty = client.getAllProduktyXml(ResponseList.class);
                
        for(Produkt p : allProdukty.getProdukty()) {
            System.out.println(p.getNazwa());
        }
        System.out.println("------------------");
        System.out.println("end");
        
        System.out.println("\n znajdz produkty o nazwie: mysz");
        System.out.println("------------------");
        allProdukty = client.getProducts((ResponseList.class), "mysz");
        for(Produkt p : allProdukty.getProdukty()) {
            System.out.println(p.getNazwa()+" "+p.getProducent()+" "+String.valueOf(p.getCena()));
        }
        System.out.println("------------------");
        System.out.println("end");
        
//        Produkt p1 = new Produkt(1L, "komputer", 2000, "dell");
//        Produkt p2 = new Produkt(2L, "telefon", 1000, "samsung");
//        Produkt p3 = new Produkt(3L, "mysz", 150, "acer");
        
        System.out.println("\n findProducts (mysz, acer, 1000 zl)");
        System.out.println("------------------");
        
//        SearchParam searchParam1 = new SearchParam();
//        searchParam1.setNazwa("mysz");
//        searchParam1.setProducent("acer");
//        searchParam1.setCenaMniejszaNiz(200);
//        
//        ResponseList searched = client.findProducts(Entity.json(searchParam1), ResponseList.class);
        
        ResponseList searched = client.findSearchProducts("mysz", "acer", 1000);

//        ResponseList searched = client.searchList();
        
        for(Produkt p : searched.getProdukty()) {
            System.out.println(p.getNazwa());
        }
        System.out.println("------------------");
        
        
    
                
    }
    
}
