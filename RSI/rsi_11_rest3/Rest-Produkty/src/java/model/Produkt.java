/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public class Produkt {    
    private long id;
    private String nazwa;
    private int cena;    
    private String producent;
    
    public Produkt(){        
    }
    
    public Produkt(long id, String nazwa, int cena, String producent) {
        this.id = id;
        this.nazwa = nazwa;
        this.cena = cena;
        this.producent = producent;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the nazwa
     */
    public String getNazwa() {
        return nazwa;
    }

    /**
     * @param nazwa the nazwa to set
     */
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    /**
     * @return the cena
     */
    public int getCena() {
        return cena;
    }

    /**
     * @param cena the cena to set
     */
    public void setCena(int cena) {
        this.cena = cena;
    }

    /**
     * @return the producent
     */
    public String getProducent() {
        return producent;
    }

    /**
     * @param producent the producent to set
     */
    public void setProducent(String producent) {
        this.producent = producent;
    }
    
    
}
