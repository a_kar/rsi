/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alicj
 */
@XmlRootElement
class SearchParam {
    
    private String nazwa;
    private String producent;
    private int cenaMniejszaNiz;

//    public SearchParam(String nazwa, String producent, int cenaMniejszaNiz) {
//        this.nazwa = nazwa;
//        this.producent = producent;
//        this.cenaMniejszaNiz = cenaMniejszaNiz;
//    }
    /**
     * @return the nazwa
     */
    public String getNazwa() {
        return nazwa;
    }

    /**
     * @param nazwa the nazwa to set
     */
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    /**
     * @return the producent
     */
    public String getProducent() {
        return producent;
    }

    /**
     * @param producent the producent to set
     */
    public void setProducent(String producent) {
        this.producent = producent;
    }

    /**
     * @return the cenaMniejszaNiz
     */
    public int getCenaMniejszaNiz() {
        return cenaMniejszaNiz;
    }

    /**
     * @param cenaMniejszaNiz the cenaMniejszaNiz to set
     */
    public void setCenaMniejszaNiz(int cenaMniejszaNiz) {
        this.cenaMniejszaNiz = cenaMniejszaNiz;
    }
    
    
}
