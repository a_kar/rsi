/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import model.Produkt;
import model.Produkty;
/**
 *
 * @author alicj
 */
@Path("/sklep")
public class SklepResource {
    
    private Produkty produkty = new Produkty();
    
    @Context
    private UriInfo context;
    
    public SklepResource() {}
    
    @GET
    @Path("/allproducts")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseList getAllProdukty() {
        List<Produkt> allProdukty = produkty.getAllProdukty();
        ResponseList responseList = new ResponseList();
        responseList.setList(allProdukty);
        return responseList;
    }
    
    
}
