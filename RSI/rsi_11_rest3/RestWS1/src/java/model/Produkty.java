/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Produkt;
/**
 *
 * @author alicj
 */
public class Produkty {
    static private Map<Long, Produkt> produkty = new HashMap<Long, Produkt>();
    
    public Produkty() {
        Produkt p1 = new Produkt(1L, "komputer", 2000, "dell");
        Produkt p2 = new Produkt(2L, "telefon", 1000, "samsung");
        Produkt p3 = new Produkt(3L, "mysz", 150, "acer");
       
        produkty.put(1L, p1);
        produkty.put(2L, p2);
        produkty.put(3L, p3);
    }
    
    public List<Produkt> getAllProdukty() {
        return new ArrayList<Produkt>(produkty.values());
    }
}
