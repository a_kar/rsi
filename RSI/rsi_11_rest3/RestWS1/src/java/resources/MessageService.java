/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Message;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author alicj
 */
public class MessageService {
    
    static private Map<Long, Message> messages = new HashMap<Long, Message>();
    
    public MessageService() {
        Message m1 = new Message(1L, "Pierwsza wiad", "Jacek");
        Message m2 = new Message(2L, "Druga wiad", "Marek");
        Message m3 = new Message(3L, "Trzecia wiad", "Ewa");
        
        messages.put(1L, m1);
        messages.put(2L, m2);
        messages.put(3L, m3);
    }
    
    public List<Message> getAllMessages() {
        return new ArrayList<Message>(messages.values());
    }
    
    public Message getMessage(Long id){
        return messages.get(id);
    }
    
    public Message createMessage(Message message) {
        message.setID(messages.size() + 1L);
        messages.put(messages.size() + 1L, message);
        return message;
    }

    public Message updateMessage(Message message) {
        messages.put(message.getID(), message);
        return message;
    }

    public void deleteMessage(Long id) {
        messages.remove(id);
    }

    public List<Message> getAllMessagesStartingWith(String par1) {
        System.out.println("getAllMessagesStartingWith:"+par1);
        List<Message> list = new ArrayList<Message>();
        for (Message msg : messages.values()) {
            if(msg.getMessage().toLowerCase().indexOf(par1.toLowerCase()) == 0) {
                list.add(msg);
            }
        }
        return list;
    }

    public List<Message> getMessagesByAuthor(String author) {
        System.out.println("getMessagesByAuthor:"+author);
        List<Message> list = new ArrayList<Message>();
        for (Message msg : messages.values()) {
            if(msg.getAuthor().toLowerCase().equals(author.toLowerCase())) {
                list.add(msg);
            }
        }
        return list;
    }

    
}
