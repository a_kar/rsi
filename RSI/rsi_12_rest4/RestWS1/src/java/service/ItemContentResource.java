package service;

import model.Message;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class ItemContentResource {
    
    Message message;
    
    public ItemContentResource(Message message) {
        this.message = message;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Message get() {
        return this.message;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("comments")
    public String getComments() {
        return "Comments for product: " + this.message.getAuthor() + ", " + this.message.getMessage();
    }
    
//    @GET
//    @Produces("text/plain")
//    public String get() {
//        return "comments to message";
//    }
    
}