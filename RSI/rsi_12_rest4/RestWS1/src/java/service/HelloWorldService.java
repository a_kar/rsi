/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;

/**
 * REST Web Service
 *
 * @author alicj
 */
@Path("/hello")
public class HelloWorldService {

    @Context
    private UriInfo context;

    
    public HelloWorldService() {
    }

    
    @GET    
    @Produces(javax.ws.rs.core.MediaType.TEXT_HTML)
    public String getHtml() {
        return "Witaj swiecie";
    }
    
    @GET
    @Produces("text/plain")
    public String getHello() {
        return "get hello";
    }
    
    
    @Path("/echo")
    @GET
    @Produces(javax.ws.rs.core.MediaType.TEXT_HTML)
    public String getHtmlEcho() {
        return "Witaj Echo";
    }
    
    @GET
    @Path("/echo2/{parametr}")
    public String echo(@PathParam("parametr") String name) {
        return "Witaj Echo: "+name;
    }


    /**
     * PUT method for updating or creating an instance of HelloWorldService
     * @param content representation for the resource
     */
    @PUT
    @Consumes(javax.ws.rs.core.MediaType.TEXT_HTML)
    public void putHtml(String content) {
    }
}
