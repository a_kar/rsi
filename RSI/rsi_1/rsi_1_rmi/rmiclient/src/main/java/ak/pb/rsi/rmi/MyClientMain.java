/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.rmi;

/**
 *
 * @author alicj
 */
import java.rmi.Naming;

public class MyClientMain {

    public static void main(String[] args) {

        System.setProperty("java.security.policy", "security.policy");

        System.setSecurityManager(new SecurityManager());

        try {

            //MyServerInt myRemoteObject = (MyServerInt) Naming.lookup("rmi://localhost:1099/hello");
            
            MyServerInt myRemoteObject = (MyServerInt) Naming.lookup("//192.168.0.198:1099/ABC");
            //   MyServerInt myRemoteObject = (MyServerInt) Naming.lookup("rmi://localhost/ABC");
            String text = "Hello :-)";

            String result = myRemoteObject.getDescription(text);

            System.out.println("Wysłano do serwera: " + text);

            System.out.println("Otrzymana z serwera odpowiedź: " + result);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}





//import java.net.MalformedURLException;
//import java.rmi.Naming;
//import java.rmi.NotBoundException;
//import java.rmi.RemoteException;
//
//import java.net.MalformedURLException;
//import java.rmi.Naming;
//import java.rmi.NotBoundException;
//import java.rmi.RemoteException;
//
//public class MyClientMain {
//
//    public static void main(String[] args) 
//            throws NotBoundException, MalformedURLException, RemoteException{
//
//        MyServerInt myRemoteObject = (MyServerInt) Naming.lookup("rmi://localhost:1099/hello");
//        //String text = "Hallo :-)";
//        //String result = myRemoteObject.getDescription(text);
//        System.out.println("Wysłano do servera: " + myRemoteObject.getDescription("Hi! "));
//        //System.out.println("Otrzymana z serwera odpowiedź: " + result);
//
//    }}
