/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author alicj
 */
public interface MyServerInt extends Remote {
    
    public String getDescription (String text) throws RemoteException;
}
