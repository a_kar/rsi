/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.rmi;

/**
 *
 * @author alicj
 */
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class MyServerMain {


public static void main (String[] argv) {
    try {
            System.setProperty("java.security.policy", "security.policy");
            
            System.setProperty("java.rmi.server.codebase", "http://192.168.0.198/RMIserver/");
            System.setProperty("java.rmi.server.hostname", "192.168.0.198");
                
            //System.setProperty("java.rmi.server.codebase", "file:/D:/NetBeansProjects/rsi_1_rmi/rmihello/build/classes/ak/pb/rsi/rmi/");
            //build/classes/
            //target/classes/
            System.out.println("Codebase: " + System.getProperty("java.rmi.server.codebase"));
            Registry registry = LocateRegistry.createRegistry(1099);
            
            //registry.rebind("hello", new MyServerImpl());
            
            MyServerImpl obj1 = new MyServerImpl();

            //Naming.rebind("//localhost/ABC", obj1);
            
            Naming.rebind("//192.168.0.198/ABC", obj1);
            
            //---
            System.out.println("Server is ready.");
            }catch (Exception e) {
                    System.out.println("HelloImpl err: " + e.getMessage());
                 }
    }

}
