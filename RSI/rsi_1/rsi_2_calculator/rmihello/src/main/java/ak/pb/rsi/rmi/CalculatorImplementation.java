/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.rmi;

/**
 *
 * @author alicj
 */
public class CalculatorImplementation 
        extends java.rmi.server.UnicastRemoteObject 
    implements Calculator { 
  
    public CalculatorImplementation() 
        throws java.rmi.RemoteException { 
        super(); 
    } 
 
    @Override
    public long add(long a, long b) 
        throws java.rmi.RemoteException { 
        return a + b; 
    } 
 
    @Override
    public long sub(long a, long b) 
        throws java.rmi.RemoteException { 
        return a - b; 
    } 
 
    @Override
    public long mul(long a, long b) 
        throws java.rmi.RemoteException { 
        return a * b; 
    } 
 
    @Override
    public long div(long a, long b) 
        throws java.rmi.RemoteException { 
        return a / b; 
    } 
} 

