/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi.client;

import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;

import ak.pb.rsi.ShopInfo;

public class Client2{
    
    private static final String WS_URL = "http://localhost:8080/WebServiceGlass/ShopInfoService?wsdl";
        
    public static void main(String[] args) throws Exception {
       
    URL url = new URL(WS_URL);
        QName qname = new QName("http://rsi.pb.ak/", "ShopInfo");

        Service service = Service.create(url, qname);
        ShopInfo shop = service.getPort(ShopInfo.class);
        
        /*******************UserName & Password ******************************/
        Map<String, Object> req_ctx = ((BindingProvider)shop).getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, WS_URL);

        Map<String, List<String>> headers = new HashMap<String, List<String>>();
//        headers.put("Username", Collections.singletonList("alicja_user1"));
//        headers.put("Password", Collections.singletonList("password"));
        headers.put("name", Collections.singletonList("Alicja"));
        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        /**********************************************************************/
        
        System.out.println(shop.getShopInfo(WS_URL));
       
    }
}
