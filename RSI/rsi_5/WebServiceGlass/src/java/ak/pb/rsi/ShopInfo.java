/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.pb.rsi;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author alicj
 */
@WebService
public class ShopInfo {
    
    @WebMethod
    public String getShopInfo(String name) throws InvalidInputException {
        String outputText = "Jest OK";
        if(name.equals("Alicja")){
            outputText = "Akceptuje " + name;
        }
        else {
            throw new InvalidInputException ("Niewlasciwe dane wejsciowe", 
                    name + " nazwa jest niepoprawna" );
        }
        return outputText; 
    }
}
