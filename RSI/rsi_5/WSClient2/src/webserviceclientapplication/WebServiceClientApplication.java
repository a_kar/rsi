package webserviceclientapplication;
import ak.pb.rsi.InvalidInputException_Exception;

public class WebServiceClientApplication {

    public static void main(String[] args) {
        {
            try {
                java.lang.String name = "Alicja1";
                java.lang.String result = getShopInfo(name);
                System.out.println("Result = " + result);
            } 
        
            catch (Exception ex) {System.out.println("Exception: " + ex);
        }}
    }


    private static String getShopInfo(java.lang.String arg0) throws InvalidInputException_Exception {
        ak.pb.rsi.ShopInfoService service = new ak.pb.rsi.ShopInfoService();
        ak.pb.rsi.ShopInfo port = service.getShopInfoPort();
        return port.getShopInfo(arg0);
    }
    
    

    
}
