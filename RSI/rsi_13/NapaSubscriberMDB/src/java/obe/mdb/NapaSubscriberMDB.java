/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obe.mdb;

import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 *
 * @author alicj
 */
@MessageDriven(mappedName = "jms/myTopic", activationConfig = {
    @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "Region = 'Napa Valley' OR Region = 'All Regions'"),
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "jms/myTopic"),
    @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "jms/myTopic"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
})
public class NapaSubscriberMDB implements MessageListener {
    
    public NapaSubscriberMDB() {
    }
    
    @Override
    public void onMessage(Message message) {
        if (message instanceof MapMessage) {
            MapMessage mm = (MapMessage) message;
            String request = null;
            try {
                Enumeration<String> mapNames = mm.getMapNames();
                while (mapNames.hasMoreElements()) {
                    // Read all of the map elements as strings
                    String name = mapNames.nextElement();
                    request += name + ": " + mm.getString(name) + " | ";
                }
                System.out.println(this.getClass().getName() + ": Received a request for " + message.getStringProperty("Region"));
                System.out.println(request);
            } catch (JMSException ex) {
                System.out.println("Failed to get request message");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
