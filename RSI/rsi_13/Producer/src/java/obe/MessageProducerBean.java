/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obe;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Named(value = "messageProducerBean")
@RequestScoped
public class MessageProducerBean {

    @Resource(mappedName = "jms/myQueue")
    private Queue myQueue;

    @Inject
    @JMSConnectionFactory("jms/myQueueFactory")
    private JMSContext context;
    private String message;
    
    public MessageProducerBean() {}
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public void send() {
        System.out.println("Test - MessageProducerBean send: " + message);
        sendJMSMessageToMyQueue(message);
        
        FacesContext facesContext = FacesContext.getCurrentInstance();
        FacesMessage facesMessage = new FacesMessage("Message sent to jms: " + message);
        facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
        facesContext.addMessage(null, facesMessage);
    }

    private void sendJMSMessageToMyQueue(String messageData) {
        System.out.println("Sending message to jms/myQueue " + message);
        context.createProducer().send(myQueue, messageData);
    }
    
    
}
