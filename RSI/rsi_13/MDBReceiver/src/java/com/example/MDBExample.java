package com.example;

import java.util.logging.Logger;
import java.util.logging.Level;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import sun.util.logging.PlatformLogger;
 
@MessageDriven(mappedName = "jms/myQueue", activationConfig = {
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class MDBExample implements MessageListener {

    public MDBExample() {
    }

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage tm = (TextMessage) message;
            System.out.println("Consumed message: " + tm.getText());
        } catch (JMSException ex) {
            System.out.println("JMSException: " + ex);
            Logger.getLogger(MDBExample.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
}
