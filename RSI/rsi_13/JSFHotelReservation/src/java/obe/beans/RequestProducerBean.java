package obe.beans;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import obe.model.ReservationRequest;

@Named(value = "request")
@Dependent
public class RequestProducerBean implements Serializable {

    @Resource(mappedName = "jms/myTopic")
    private Topic myTopic;
    @Resource(mappedName = "jms/myTopicFactory")
    private ConnectionFactory myTopicFactory;
    
    public RequestProducerBean() {
    }
    
    public void send(ReservationRequest request, String region) throws JMSException {
        Connection connection = null;
        Session session = null;
        try {
            connection = myTopicFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(myTopic);
            MapMessage mm = session.createMapMessage();
            mm.setString("LastName", request.getLastName());
            mm.setString("FirstName", request.getFirstName());
            mm.setString("EMail", request.getEmail());
            mm.setString("DateRequested", request.getDateRequested());
            mm.setInt("NumberOfNights", request.getNumberOfNights());
            mm.setInt("NumberInParty", request.getNumberInParty());
            mm.setStringProperty("Region", region);
            messageProducer.send(mm);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Cannot close session", e);
                }
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
    
}
