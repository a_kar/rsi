package obe.model;

public class ReservationRequest {
    private String lastName, firstName;
    private String email;
    private String dateRequested;
    private int numberOfNights;
    private int numberInParty;

    public ReservationRequest(String lastName, String firstName, String email, String dateRequested, int numberOfNights, int numberInParty) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.dateRequested = dateRequested;
        this.numberOfNights = numberOfNights;
        this.numberInParty = numberInParty;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(String dateRequested) {
        this.dateRequested = dateRequested;
    }

    public int getNumberOfNights() {
        return numberOfNights;
    }

    public void setNumberOfNights(int numberOfNights) {
        this.numberOfNights = numberOfNights;
    }

    public int getNumberInParty() {
        return numberInParty;
    }

    public void setNumberInParty(int numberInParty) {
        this.numberInParty = numberInParty;
    }
    
    
    
}
