/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import resources.MessageService;
import model.Message;

/**
 *
 * @author alicj
 */
@Path("/messages")
public class MessageResource {
    
    MessageService messageService = new MessageService();
    
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<Message> getText() {
        return messageService.getAllMessages();
    }
    
    @Path("/json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Message> getTextJson() {
        return messageService.getAllMessages();
    }
    
}
