/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.net.URI;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import resources.MessageService;
import model.Message;

/**
 *
 * @author alicj
 */
@Path("/messages")
public class MessageResource {
    
    MessageService messageService = new MessageService();
    
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<Message> getText() {
        return messageService.getAllMessages();
    }
    
    @Path("/json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Message> getTextJson() {
        return messageService.getAllMessages();
    }
        
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Message> getMessages(@QueryParam("zaczynasie") String par1) {
        if (par1 != null){
            return messageService.getAllMessagesStartingWith(par1);
        }
        return messageService.getAllMessages();
    }
    
    @Path("/uri-info")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getUriInfo(@Context UriInfo uriInfo) {        
        return uriInfo.getAbsolutePath().toString();
    }
    
    @Path("/headers")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getHeaders(@Context HttpHeaders headers) {        
        HttpHeaders httpHeaders;
        String headersinfo = "";
        MultivaluedMap<String, String> requestHeaders = headers.getRequestHeaders();
        for (Map.Entry<String, List<String>> entry : requestHeaders.entrySet()) {
            headersinfo += (entry.getKey() + "=" + entry.getValue() +"\n");
        }        
        return headersinfo;
    }
    
    @GET
    @Path("/{messageId}")
    public Message getMessage(@PathParam("messageId") Long id) {
        return messageService.getMessage(id);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Message createMessage(Message message) {
        return messageService.createMessage(message);
    }
    
    @PUT
    @Path("/{messageId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Message updateMessage(Message message) {
        return messageService.updateMessage(message);
    }
    
    @DELETE
    @Path("/{messageId}")
    public void deleteMessage(@PathParam("messageId") Long id) {
        messageService.deleteMessage(id);
    }
    
    @Path("/author")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Message> getMessagesByAuthor(@MatrixParam("author") String author) {

        if (author != null){
            return messageService.getMessagesByAuthor(author);
        }
        return messageService.getAllMessages();

    }
    
    @GET
    @Path("test3")
    public String requestParamTest1(@HeaderParam(HttpHeaders.HOST) String host) {
        return "The request 'host' header value = " + host;
    }
    

}
