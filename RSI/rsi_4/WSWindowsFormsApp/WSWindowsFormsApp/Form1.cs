﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSWindowsFormsApp.SklepLista;

namespace WSWindowsFormsApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, EventArgs e)
        {
            SklepLista.SklepClient client = new SklepLista.SklepClient();
            results.Text = "";
            foreach ( produkt i in client.listaProduktow() )
            {
                results.Text += "nazwa: " + i.nazwa + "\t opis: " + i.opis + "\t cena: " + i.cena;
                results.Text += "\n\n";
            }

        }

    }
}
