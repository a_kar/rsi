/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webserviceclientapplication;

import ak.pb.rsi.InvalidInputException_Exception;

/**
 *
 * @author alicj
 */
public class WebServiceClientApplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        {
            try {
//                java.lang.String ip = "212.33.68.5"; // - pb.edu.pl
//                java.lang.String result = getIpLocation(ip);
//                System.out.println("Result = " + result);
                java.lang.String name = "Alicja";
                java.lang.String result = getShopInfo(name);
                System.out.println("Result = " + result);
            } 
        
            catch (Exception ex) {System.out.println("Exception: " + ex);
        }}
    }

    private static String getIpLocation(java.lang.String sIp) {
        com.lavasoft.GeoIPService service = new com.lavasoft.GeoIPService();
        com.lavasoft.GeoIPServiceSoap port = service.getGeoIPServiceSoap();
        return port.getIpLocation(sIp);
    }

    private static String getShopInfo(java.lang.String arg0) throws InvalidInputException_Exception {
        ak.pb.rsi.ShopInfoService service = new ak.pb.rsi.ShopInfoService();
        ak.pb.rsi.ShopInfo port = service.getShopInfoPort();
        return port.getShopInfo(arg0);
    }
    
    

    
}
