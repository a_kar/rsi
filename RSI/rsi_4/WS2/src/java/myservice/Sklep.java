/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myservice;
import datamodel.Produkt;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class Sklep {
    
    @WebMethod
    public List<Produkt> listaProduktow() {
        List<Produkt> arrayList = new ArrayList<Produkt>();
        
        arrayList.add(new Produkt("mysz", "Dell", 100));
        arrayList.add(new Produkt("monitor", "Dell", 1600));
        arrayList.add(new Produkt("laptop", "Acer", 3100));
        
        return arrayList;
    }
    
    
}
