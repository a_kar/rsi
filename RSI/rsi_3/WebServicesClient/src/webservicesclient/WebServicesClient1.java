package webservicesclient;

import ak.pb.rsi.rmi.HelloWorld;
import ak.pb.rsi.rmi.HelloWorldImplService;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class WebServicesClient1 {

    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("http://localhost:9999/ws/hello?wsdl");
 //       URL url = new URL("http://localhost:8080/ws/hello?wsdl");
        QName qname = new QName("http://rmi.rsi.pb.ak/",
            "HelloWorldImplService");
        
        Service service = Service.create(url, qname);
        HelloWorld hello = service.getPort(HelloWorld.class);

        String zapytanie = "To ja - klient 2";
        String response = hello.getHelloWorldAsString(zapytanie);
        System.out.println("Klient wysłał: " + zapytanie);
        System.out.println("Klient otrzymał: " + response);   
    }
    
}
