package webservicesclient;

import ak.pb.rsi.rmi.HelloWorld;
import ak.pb.rsi.rmi.HelloWorldImplService;

public class WebServicesClient0 {
    
    public static void main(String[] args) {
        HelloWorldImplService  helloService = new HelloWorldImplService();
    
        HelloWorld port = helloService.getHelloWorldImplPort();

        String zapytanie = "To ja - klient";
        String response = port.getHelloWorldAsString(zapytanie);
        System.out.println("Klient wysłał: " + zapytanie);
        System.out.println("Klient otrzymał: " + response);   
    }         
}
