package ak.pb.rsi.rmi;

import javax.jws.WebService;

//Service Implementation 
@WebService(endpointInterface = "ak.pb.rsi.rmi.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
    
    @Override
    public String getHelloWorldAsString(String name) {
        return "Witaj swiecie JAX-WS: " + name;
    }
}
