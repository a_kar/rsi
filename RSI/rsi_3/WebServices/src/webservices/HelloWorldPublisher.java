package webservices;

import javax.xml.ws.Endpoint;
import ak.pb.rsi.rmi.HelloWorldImpl;

public class HelloWorldPublisher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9999/ws/hello",
                new HelloWorldImpl());
        System.out.println("Web Service czeka na klienta...");
        
    }
    
}
