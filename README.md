# README #

### Zadania programistyczne tworzone w ramach przedmiotu dotyczącego rozproszonych systemów internetowych. ###



1. Java RMI – uruchamianie przykładów i pisanie aplikacji z Java RMI.
2. Zagadnienia z SOAP web serwisów: uruchamianie web serwisów z wykorzystaniem specyfikacji JAX-WS, monitorowanie komunikatów SOAP, analiza WSDL, implementacja WS na serwerze, tworzenie aplikacji klienckiej, użycie SoapUI, testowanie wpływu adnotacji JAX-WS na WSDL, handlery, implementacja autentyfikacji, użycie WS-Security i SSL, generowanie wyjątków, przesyłanie załączników binarnych, pisanie projektów.
3. Zagadnienia z RESTful web serwisów: tworzenie web serwisów z wykorzystaniem specyfikacji JAX-RS, testowanie adnotacji, generowanie różnych reprezentacji zasobów, testowanie web serwisów z użyciem aplikacji Postman, budowa aplikacji funkcjonalności CRUD, kontekst, tworzenie klienta na bazie biblioteki Jersey, WADL, monitorowanie zapytań i odpowiedzi, definicja podzasobów, implementacja HATEOAS, implementacja filtrów, pisanie projektów.
4. Implementacja aplikacji korzystającej z enterprise messaging systems na bazie JMS.